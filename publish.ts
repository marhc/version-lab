const { exec } = require("child_process");

const versionCommand = `npm version ${process.env.VERSION_COMMAND} -f --allow-same-version -no-git-tag-version`;

exec(versionCommand, (error: string, stdout: string, stderr: string) => {
    if (error) {
        console.log(`error: ${error}`);
        return;
    }
    if (stderr && !stderr.match('Recommended protections')) {
        console.log(`stderr: ${stderr}`);
        return;
    }
    console.log(`Nova Versão: ${stdout}`);
});
